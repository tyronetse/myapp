package com.mycompany.myapp.workflow.process;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.resource.ResourceResolver;
import javax.jcr.Session;

import com.adobe.granite.workflow.WorkflowException;
import com.adobe.granite.workflow.WorkflowSession;
import com.adobe.granite.workflow.exec.ParticipantStepChooser;
import com.adobe.granite.workflow.exec.WorkItem;
import com.adobe.granite.workflow.metadata.MetaDataMap;

@Component
@Service
@Properties({
@Property(name = Constants.SERVICE_DESCRIPTION, value = "Get the particpant user or group from the Workflow property in the MetaData Map"),
@Property(name = ParticipantStepChooser.SERVICE_PROPERTY_LABEL, value = "Workflow MetaData Particpant Workflow Chooser") })
public class DynamicParticipantChooser implements ParticipantStepChooser {

	final String DEFAULT_PROPERTY = "approver";

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	public String getParticipant(WorkItem witem, WorkflowSession wfsession,
		MetaDataMap metaData) throws WorkflowException {

		String workflowProperty           = metaData.get("PROCESS_ARGS", DEFAULT_PROPERTY);

		Session session                   = wfsession.adaptTo(Session.class);
		ResourceResolver resourceResolver = wfsession.adaptTo(ResourceResolver.class);

		String default_assignee="";

        try {

            //Get the MetaDataMap of the workflow
            MetaDataMap wfd = witem.getWorkflow().getWorkflowData().getMetaDataMap();

            if (wfd.containsKey(workflowProperty))
            {
                default_assignee = wfd.get(workflowProperty, String.class);

            }
            else
            {
            	default_assignee = "admin";
            }
        }
        catch (Exception e) {
        	logger.error("DynamicParticipantChooser:Exception {}", e.toString());
        }


        return default_assignee;

	}

}