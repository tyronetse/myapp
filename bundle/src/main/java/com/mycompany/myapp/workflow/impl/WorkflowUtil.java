package com.mycompany.myapp.workflow.impl;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.day.cq.commons.jcr.JcrConstants;
import com.day.cq.workflow.WorkflowException;
import com.day.cq.workflow.WorkflowService;
import com.day.cq.workflow.WorkflowSession;

import com.day.cq.workflow.exec.Workflow;
import com.day.cq.workflow.exec.WorkflowData;
import com.day.cq.workflow.metadata.MetaDataMap;
import com.day.cq.workflow.model.WorkflowModel;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;

import javax.jcr.RepositoryException;
import javax.jcr.Session;
import static com.day.cq.commons.jcr.JcrConstants.JCR_PATH;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WorkflowUtil {

	private static Logger log = LoggerFactory.getLogger(WorkflowUtil.class);

    public static boolean kickOffWorkFlow(Session session, String contentPath, String workflowModelId, WorkflowService workflowService, Map <String, String> parameters)
    {
        boolean         wfResult   = true;
        WorkflowSession wfSession;
        try {
            wfSession = workflowService.getWorkflowSession(session);
            Set<String> inRunningSet = genInRunningNodeSet(wfSession);
            submitJob(wfSession, contentPath, workflowModelId, inRunningSet, parameters);
        } catch (Exception e) {
            log.error("Error while treating events", e);
            //log.error "Error while treating events", e
            wfResult=false;
        }
        finally {
            /*
             * if (wfSession != null)
             * 		wfSession.logout();
             */
        }
        return wfResult;
    }
    /**
     * Generate in running node set
     * @param wfSession : the set of workflows to check for running nodes
     * @return list of running workflows in the workflow session
     * @throws WorkflowException
     * @throws RepositoryException
     */
	private static Set<String> genInRunningNodeSet(WorkflowSession wfSession) throws WorkflowException, RepositoryException
	{
		Set<String> inWorkflowPaths = new HashSet<String>();

    	for (Workflow wf : wfSession.getWorkflows(new String[] { "RUNNING" }))
    	{
    		WorkflowData data = wf.getWorkflowData();
    		if (JCR_PATH.equals(data.getPayloadType()))
    			inWorkflowPaths.add(data.getPayload().toString());
        }

	    log.debug("List of calendar event node currently in workflow: {}", inWorkflowPaths);
	    return inWorkflowPaths;
	}

    /**
     * Submits a job workflow
     * @param wfSession The workflow in use
     * @param e : the event for the workflow
     * @param wfModelId : what model of workflow to run
     * @param inRunningSet : the set of running workflows in this workflow's set
     * @throws RepositoryException
     * @throws WorkflowException
     */
    private static void submitJob(WorkflowSession wfSession, String nodePath, String wfModelId, Set<String> inRunningSet, Map <String, String> parameters) throws RepositoryException, WorkflowException
    {

        //TODO: Determine if there is ever a case where we need a WF to be exclusive (one running at a time); reimplement this accordingly
        //if(!inRunningSet.contains(nodePath)) {
            startWorkflow(wfSession, wfModelId, nodePath, parameters);
        //}
    }
    /**
     * Starts a Workflow
     * @param wfSession : which workflow session the wf will start in
     * @param wfModelId : the type of workflow to start
     * @param identifier : the serialized ID of the starting workflow
     * @throws RepositoryException
     * @throws WorkflowException
     */
    private static void startWorkflow(WorkflowSession wfSession, String wfModelId, String identifier, Map <String, String> parameters) throws RepositoryException, WorkflowException
    {
        WorkflowModel model = wfSession.getModel(wfModelId);
        if (model != null)
        {
            WorkflowData data = wfSession.newWorkflowData("JCR_PATH", identifier);

            //How to store data in the workflow model MetaDataMap


            //Add parameters to Workflow MetaDataMap
            MetaDataMap mdp=data.getMetaDataMap();
            mdp.putAll(parameters);

            wfSession.startWorkflow(model, data);
        } else {
            log.warn("Unknown workflow model with id: {}", wfModelId);
        }
    }


    /**
     * Gets JCR Properties specified in comma delimitted list
     * @param propertyNameList : Comma delimitted list of property names
     * @param contentPath : Path in the JCR
     * @param ResourceResolver : ResourceResolver
     *
     * @return Map : Map of the properties
     */
    public static Map <String, String> getJCRProperties (String propertyNameList, String contentPath, ResourceResolver resourceResolver)
    {
        //Get property names
        String [] propertyNames = propertyNameList.split(",");
        String jcrValue="";
        Map <String, String> jcrProperties = new HashMap<String, String>();
        final Resource res = resourceResolver.getResource(contentPath);
        final Resource resContent = resourceResolver.getResource(res,JcrConstants.JCR_CONTENT);
        if (res!=null)
        {
            for (String key : propertyNames)
            {

                ValueMap properties = res.adaptTo(ValueMap.class);
                ValueMap jcrContentProperties = resContent.adaptTo(ValueMap.class);
                if (properties.containsKey(key))
                {
                    jcrValue = properties.get(key, String.class);

                    jcrProperties.put(key,jcrValue);

                }else if (jcrContentProperties.containsKey(key))
                {
                    jcrValue = jcrContentProperties.get(key, String.class);
                    jcrProperties.put(key,jcrValue);

                }
            }
        }
        return jcrProperties;
    }
    /**
     * Gets JCR Property specified in propertyName parameter
     * @param propertyName : Property name
     * @param contentPath : Path in the JCR
     * @param ResourceResolver : ResourceResolver
     * @return String : String value of the property
     */
    public static String getJCRProperty (String propertyName, String contentPath, ResourceResolver resourceResolver)
    {
        final Resource res = resourceResolver.getResource(contentPath);
        ValueMap properties = res.adaptTo(ValueMap.class);

        //Property is not in jcr:content node

        if (properties.containsKey(propertyName))
        {
            return properties.get(propertyName, String.class);
        }
        else
        {
            return null;
        }
    }


}
