package com.mycompany.myapp.servlets;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.jcr.Session;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

import com.day.cq.workflow.WorkflowService;
import com.mycompany.myapp.workflow.impl.WorkflowUtil;

@SuppressWarnings("serial")
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = "wf", extensions = "json", methods = {
		"GET", "POST" }, generateService = true, generateComponent = true, metatype = true, label = "Workflow Servlet")
public class WorkflowServlet extends SlingAllMethodsServlet {

	private static final Logger log = LoggerFactory
			.getLogger(WorkflowServlet.class);

    @Reference
    private WorkflowService workflowService;

    @Override
    public final void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        try {
			doMethod(request, response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public
    final void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        try {
			doMethod(request, response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }


    public final void doMethod(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException, JSONException {

         final ResourceResolver resourceResolver = request.getResourceResolver();
         final Session          session              = resourceResolver.adaptTo(Session.class);
         final UserManager      userManager          = resourceResolver.adaptTo(UserManager.class);
    	 JSONObject  data;

         // Data incoming as JSON

         try
         {
             data= new JSONObject(request.getReader().readLine());
         }
         catch  (Exception e) {

             data= new JSONObject("{}");
         }

         String command = request.getParameter("command");

         if (command==null)
         {
             command=data.getString("command");
         }

         if(command.equals("approveWorkflow"))
         {
             String contentPath = request.getParameter("contentpath");

             if (contentPath==null)
             {
            	 contentPath=data.getString("contentpath");
             }


             String approver = request.getParameter("approver");

             if (approver==null)
             {
            	 approver=data.getString("approver");
             }

             boolean startWorkflowResult=false;

             String workflowModel ="/etc/workflow/models/demo-approval-workflow/jcr:content/model";

             //New put into workflow metadata
             Map <String, String> wfParameters = new HashMap<String, String>();
             wfParameters.put("approver",approver);
             startWorkflowResult=WorkflowUtil.kickOffWorkFlow(session,contentPath,workflowModel,workflowService,wfParameters);

             if (startWorkflowResult)
             {
         		// Returns the JSON Notes List
         		response.getWriter().println("OK");
             }
             else
             {
            	 response.getWriter().println("FAILED");
             }
         }

    }
}