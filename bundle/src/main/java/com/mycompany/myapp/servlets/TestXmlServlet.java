package com.mycompany.myapp.servlets;

import com.day.cq.workflow.WorkflowService;
import com.mycompany.myapp.workflow.impl.WorkflowUtil;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.apache.jackrabbit.api.security.user.UserManager;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.servlets.SlingAllMethodsServlet;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jcr.Session;
import javax.servlet.ServletException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import com.day.cq.dam.api.AssetManager;

@SuppressWarnings("serial")
@SlingServlet(resourceTypes = "sling/servlet/default", selectors = "xml", extensions = "json", methods = {
		"GET", "POST" }, generateService = true, generateComponent = true, metatype = true, label = "TestXml Servlet")
public class TestXmlServlet extends SlingAllMethodsServlet {

	private static final Logger log = LoggerFactory
			.getLogger(TestXmlServlet.class);

    @Reference
    private WorkflowService workflowService;

    @Override
    public final void doGet(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        try {
			doMethod(request, response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public
    final void doPost(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException {
        try {
			doMethod(request, response);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }


    public final void doMethod(SlingHttpServletRequest request, SlingHttpServletResponse response) throws ServletException, IOException, JSONException {

         final ResourceResolver resourceResolver = request.getResourceResolver();
         final Session          session              = resourceResolver.adaptTo(Session.class);
         final UserManager      userManager          = resourceResolver.adaptTo(UserManager.class);
         final String SOURCE_URL = "https://www.fdic.gov/news/news/press/2016/pressReleases.xml";
         try
         {
             URL sourceUrl = new URL(SOURCE_URL);
             BufferedReader in = new BufferedReader(new InputStreamReader(sourceUrl.openStream()));

             InputStream is=sourceUrl.openStream();

             String newFile = "/content/dam/fdic/test.xml" ;

             AssetManager assetMgr = resourceResolver.adaptTo(AssetManager.class);
             assetMgr.createAsset(newFile, is,"text/xml", true);

             response.getWriter().println("OK");
         }
         catch  (Exception e) {
             response.getWriter().println("FAILED");
         }

    }
}