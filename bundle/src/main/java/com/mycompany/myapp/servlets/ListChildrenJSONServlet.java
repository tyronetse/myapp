package com.mycompany.myapp.servlets;

import org.apache.sling.api.servlets.SlingAllMethodsServlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletException;

import org.apache.felix.scr.annotations.sling.SlingServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.json.JSONException;
import org.apache.sling.commons.json.JSONObject;

@SlingServlet(resourceTypes = "sling/servlet/default", selectors = "child", extensions = "json", methods = {
		"GET", "POST" }, generateService = true, generateComponent = true, metatype = true, label = "List Children JSON Property Servlet")
public class ListChildrenJSONServlet extends SlingAllMethodsServlet {

	private static final Logger log = LoggerFactory
			.getLogger(ListChildrenJSONServlet.class);

	public final void doGet(SlingHttpServletRequest request,
			SlingHttpServletResponse response) throws ServletException,
			IOException {
		// final Resource resource = request.getResource()
		final ResourceResolver resourceResolver = request.getResourceResolver();

		// Set the response type and encoding:
		response.setContentType("application/json; charset=UTF-8");
		response.setCharacterEncoding("utf-8");

		String primaryType = request.getParameter("type");
		String jcrPath = request.getParameter("path");
		Resource notesResource = resourceResolver.getResource(jcrPath);
		Iterator<Resource> children = notesResource.listChildren();
		List<JSONObject> childList = new ArrayList<JSONObject>();

		while (children.hasNext()) {

			Resource r = (Resource) children.next();

			try {
				if (r.getResourceType().equals(primaryType)) {
					JSONObject childObj = new JSONObject();

					ValueMap childProp = r.adaptTo(ValueMap.class);

					String createdBy = childProp.get("jcr:createdBy",
							String.class);
					ValueMap jcrcontentProp = r.getChild("jcr:content")
							.adaptTo(ValueMap.class);

					Set<String> entries = jcrcontentProp.keySet();
					Iterator<String> entryIter = entries.iterator();
					// System.out.println("The map contains the following associations:");
					while (entryIter.hasNext()) {

						String entryKey = (String) entryIter.next();

						// System.out.println(entryKey);
						String entryValue = jcrcontentProp.get(entryKey,
								String.class);

						// System.out.println(entryValue);
						childObj.put(entryKey, entryValue);
					}
					childObj.put("createdBy", createdBy);

					childList.add(childObj);

				}
			} catch (JSONException e) {
				log.error(
						"Exception serializing data to JSON, should not happen",
						e);

			}

		}

		// Returns the JSON Notes List
		response.getWriter().println(childList.toString());

	}
}